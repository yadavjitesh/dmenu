package com.revolutio.dmenu.dmenu;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pankajrathi on 26-Jul-15.
 */
public class OtherActivity extends Activity {
    private static final String ADDRESS_URL = "http://192.168.56.1/api/getrestinfo";
    private static final String TAG_RESTAURANT_NAME = "restaurant_name";
    private static final String TAG_RESTAURANT_MOBILE = "restaurant_mobile";
    private static final String TAG_RESTAURANT_ADDRESS_1 = "restaurant_address_1";
    private static final String TAG_RESTAURANT_ADDRESS_2 = "restaurant_address_2";
    private static final String TAG_RESTAURANT_CITY = "restaurant_city";
    private static final String TAG_RESTAURANT_STATE = "restaurant_state";
    JSONParser jsonParser = new JSONParser();

    @Override protected
    void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        /*String drestid = intent.getStringExtra("irest_id");*/
        setContentView(R.layout.activity_other);
        String drestid = intent.getStringExtra("irest_id");
        new AddressFetch().execute(drestid);
/*        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("restId", drestid));
            Log.d("Restaurant ", "Address");
            JSONObject json = jsonParser.makeHttpRequest(
                    ADDRESS_URL, "GET", params);
            String jsonStr = json.toString();
            if (jsonStr.length() != 0) {
                Log.d("Successfully Address!", json.toString());
                JSONObject jsonObj = new JSONObject(jsonStr);
                String restaurant_name = jsonObj.getString(TAG_RESTAURANT_NAME);
                String restaurant_mobile = jsonObj.getString(TAG_RESTAURANT_MOBILE);
                String restaurant_address_1 = jsonObj.getString(TAG_RESTAURANT_ADDRESS_1);
                String restaurant_address_2 = jsonObj.getString(TAG_RESTAURANT_ADDRESS_2);
                String restaurant_city = jsonObj.getString(TAG_RESTAURANT_CITY);
                String restaurant_state = jsonObj.getString(TAG_RESTAURANT_STATE);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }*/


                        /*LOGIN_URL, "POST", params);*/
        // checking log for json response
/*        TextView rest_id = (TextView) findViewById(R.id.xrestid);
        rest_id.setText(drestid);*/
        /*setContentView(R.layout.activity_other);*/
    }

    class AddressFetch extends AsyncTask<String, String, String> {
        String restaurant_name;
        String restaurant_mobile;
        String restaurant_address_1;
        String restaurant_address_2;
        String restaurant_city;
        String restaurant_state;

        @Override

        protected String doInBackground(String... drestid) {
            /*String async_drestid = drestid.toString();*/
            String async_drestid = drestid[0];
            try {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("restId", async_drestid));
                Log.d("Restaurant ", "Address");
                JSONObject json = jsonParser.makeHttpRequest(
                        ADDRESS_URL, "GET", params);
                String jsonStr = json.toString();
                if (jsonStr.length() != 0) {
                    Log.d("Successfully Address!", json.toString());
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    restaurant_name = jsonObj.getString(TAG_RESTAURANT_NAME);
                    restaurant_mobile = jsonObj.getString(TAG_RESTAURANT_MOBILE);
                    restaurant_address_1 = jsonObj.getString(TAG_RESTAURANT_ADDRESS_1);
                    restaurant_address_2 = jsonObj.getString(TAG_RESTAURANT_ADDRESS_2);
                    restaurant_city = jsonObj.getString(TAG_RESTAURANT_CITY);
                    restaurant_state = jsonObj.getString(TAG_RESTAURANT_STATE);
                    /*finish();*/
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(String message) {
            TextView t_restaurant_name = (TextView) findViewById(R.id.xrestaurant_name);
            TextView t_restaurant_address_1 = (TextView) findViewById(R.id.xrestaurant_address_1);
            TextView t_restaurant_address_2 = (TextView) findViewById(R.id.xrestaurant_address_2);
            TextView t_restaurant_city = (TextView) findViewById(R.id.xrestaurant_city);
            TextView t_restaurant_state = (TextView) findViewById(R.id.xrestaurant_state);
            TextView t_restaurant_mobile = (TextView) findViewById(R.id.xrestaurant_mobile);
            t_restaurant_name.setText(restaurant_name);
            t_restaurant_address_1.setText(restaurant_address_1);
            t_restaurant_address_2.setText(restaurant_address_2);
            t_restaurant_city.setText(restaurant_city);
            t_restaurant_state.setText(restaurant_state);
            t_restaurant_mobile.setText(restaurant_mobile);
        }
    }

/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.other, menu);
        return true;
    }*/
}
