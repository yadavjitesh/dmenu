-- add a FullNames column to DishData
ALTER TABLE "DishData" RENAME TO 'DishData_ME_TMP';
CREATE TABLE "DishData" (
"DishId"  INTEGER(10) NOT NULL,
"DishName" VARCHAR(50)NOT NULL,
"ShortDesc" VARCHAR(50), 
"LongDesc" VARCHAR(250), 
"DishPrice" INTEGER(4) NOT NULL,
"DishType" CHAR(1) NOT NULL,
"DishPic" VARCHAR(50), 
"_id" INTEGER NOT NULL,
PRIMARY KEY ("DishId")
);

INSERT INTO "DishData"  ("DishId", "DishName", "ShortDesc", "LongDesc", "DishPrice", "DishType", "DishPic", "_id") SELECT "DishId", "DishName", "ShortDesc", "LongDesc", "DishPrice", "DishType", "DishPic", "_id" FROM "DishData_ME_TMP";
DROP TABLE "DishData_ME_TMP";