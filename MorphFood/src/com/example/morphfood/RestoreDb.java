package com.example.morphfood;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class RestoreDb extends SQLiteAssetHelper  {
    private static final String DATABASE_NAME = "MorphDb.sqlite";
    private static final int DATABASE_VERSION = 2;	
    private static final String TableName = "OrderDtl";
    private static final String Column1 = "DishName";
    private static final String Column2 = "DishPrice";
    
	public RestoreDb(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

    public void DeleteIfExists() {
    	SQLiteDatabase database = this.getWritableDatabase();
    	database.execSQL("DROP TABLE IF EXISTS " + TableName);
    	
    }
    
    public void CreateOrderDtl() {
    	SQLiteDatabase database = this.getWritableDatabase();
    	database.execSQL("CREATE TABLE  " + TableName + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
    	        + Column1 + " VARCHAR(50), " + Column2 + " VARCHAR(50));");
    }    
	
}
