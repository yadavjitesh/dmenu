package com.example.morphfood;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class OrderDataAdapter extends CursorAdapter {

	@SuppressWarnings("deprecation")
	public OrderDataAdapter(Context context, Cursor cursor) {
		super(context, cursor);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		// TODO Auto-generated method stub
		TextView dish_title = (TextView) view.findViewById(R.id.order_dish_name);
		
		dish_title.setText(cursor.getString(cursor.getColumnIndex("DishName")));
		Log.d("Pdebug ", "Dish Name " + dish_title);
		
		TextView dish_price = (TextView) view.findViewById(R.id.order_price);
		dish_price.setText(cursor.getString(cursor.getColumnIndex("DishPrice")));		
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		// TODO Auto-generated method stub
		Log.d("Pdebug ", "Inflating ");
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View view = inflater.inflate(R.layout.cart_listview_structure, parent, false);
		return view;
	}

}
