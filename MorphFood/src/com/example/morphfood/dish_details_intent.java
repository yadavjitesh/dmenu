package com.example.morphfood;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;



public class dish_details_intent extends MainActivity {
	private String IDishName;
	private String IDishPrice;
	private MyDatabase db;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dish_details);
		Intent intentObject = getIntent();
		IDishName = intentObject.getStringExtra("IDishName");
		String IDishSDesc = intentObject.getStringExtra("IDishSDesc");
		IDishPrice = intentObject.getStringExtra("IDishPrice");
		String IDishLDesc = intentObject.getStringExtra("IDishLDesc");
		String IDishImage = intentObject.getStringExtra("IDishImage");
		TextView dish_title = (TextView) findViewById(R.id.DishTitle);
		dish_title.setText(IDishName);

		TextView dish_short_desc = (TextView) findViewById(R.id.DishShortDesc);
		dish_short_desc.setText(IDishSDesc);

		TextView dish_long_desc = (TextView) findViewById(R.id.DishLongDesc);
		dish_long_desc.setText(IDishLDesc);
		
		TextView dish_price = (TextView) findViewById(R.id.DishPrice);
		dish_price.setText(IDishPrice);		
		
		ImageView dish_photo = (ImageView) findViewById(R.id.DishImage);
		int id = getResources().getIdentifier("com.example.morphfood:drawable/" + IDishImage, null, null);
		dish_photo.setImageResource(id);
		
//		setContentView(R.layout.dish_details);
	}
	public void onAddDish(View v)
	{
		Log.e("Pankaj Rathi", "Inside Toast ");
		Toast toast = Toast.makeText(getApplicationContext(),
			      IDishName + " added to your order.", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
		toast.show();
		//Intent intent = new Intent(getBaseContext(), add_to_order.class);  // this will not work as it will pass context onActionItemClickListener()
		//intent.putExtra("ODishName", IDishName);
		//intent.putExtra("ODishPrice", IDishPrice);
		//startActivity(intent);
//		Log.d("Pankaj", IDishName );
		db = new MyDatabase(this);
		db.addOrderDtl(this.IDishName, this.IDishPrice);
		
//		add_to_order add_to_order_instance = new add_to_order();
//		add_to_order_instance.add_dishes(IDishName, IDishName);
		
//		return new String[] {IDishName, IDishPrice};
		
	}

	
}
