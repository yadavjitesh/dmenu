package com.example.morphfood;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

public class showFoodCart extends Activity {
	private MyDatabase db;
	ListView listView;
	
	public void onCreate(Bundle savedInstanceState) {
//		Log.d("PDebug", "Inside Class");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cart_view);
		listView = (ListView) findViewById(R.id.cart_list);
		db = new MyDatabase(this);
		Cursor CusrCart = db.getAllOrderDtl();
		OrderDataAdapter OrderDataAdapterInstance = new OrderDataAdapter(this,CusrCart);
		listView.setAdapter(OrderDataAdapterInstance);	
	}

}
