package com.example.morphfood;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class MorphDataAdapter extends CursorAdapter {
	
	@SuppressWarnings("deprecation")
	public MorphDataAdapter (Context context, Cursor cursor) {
		super(context, cursor);
		
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		// TODO Auto-generated method stub
		ImageView dish_pic = (ImageView) view.findViewById(R.id.icon);		
		String image_name = cursor.getString(cursor.getColumnIndexOrThrow("DishPic"));
//		icon_pic.setImageResource(cursor.getString(cursor.getColumnIndexOrThrow("DishPic")));
		int id = context.getResources().getIdentifier("com.example.morphfood:drawable/" + image_name, null, null);
		dish_pic.setImageResource(id);	
		
		TextView dish_title = (TextView) view.findViewById(R.id.title);
		dish_title.setText(cursor.getString(cursor.getColumnIndex("DishName")));
		
		TextView dish_sdesc = (TextView) view.findViewById(R.id.desc);
		dish_sdesc.setText(cursor.getString(cursor.getColumnIndex("ShortDesc")));
		
		TextView dish_price = (TextView) view.findViewById(R.id.pric);
		dish_price.setText(cursor.getString(cursor.getColumnIndex("DishPrice")));		
	
		
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View view = inflater.inflate(R.layout.listview_structure, parent, false);
		return view;
	}

}
