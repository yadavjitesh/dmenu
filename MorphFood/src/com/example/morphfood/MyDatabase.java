package com.example.morphfood;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class MyDatabase extends SQLiteAssetHelper {

    private static String DATABASE_NAME = "MorphDb.sqlite"; // must have file extension
    private static int DATABASE_VERSION = 2;

    public static final String TABLE_1 = "DishData";
    public static final String TABLE_2 = "OrderDtl";

//    public static final String COLUMN_1= "DishId";
//    
//    public static final String COLUMN_2= "DishName";
//    
//    public static final String COLUMN_3= "ShortDesc";
//    
//    public static final String COLUMN_4= "LongDesc";
//    
//    public static final String COLUMN_5= "DishPrice";
//    
//    public static final String COLUMN_6= "DishType";
//    
//    public static final String COLUMN_7= "DishPic";	
    
    public MyDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }    
    
    public Cursor getAllTable_1 () {
    	SQLiteDatabase database = this.getReadableDatabase();
    	return database.rawQuery("Select * from " + TABLE_1, null) ;
    }
    
    public void addOrderDtl(String ODishName, String ODishPrice)
    {
//    	Log.e("Pankaj Rathi", "Fail 1");
    	final String OrderDtl_COLUMN_1= "DishName";
    	final String OrderDtl_COLUMN_2= "DishPrice";
    	
    	SQLiteDatabase database = this.getWritableDatabase();
    	ContentValues values = new ContentValues();
    	values.put(OrderDtl_COLUMN_1, ODishName );
    	values.put(OrderDtl_COLUMN_2, ODishPrice );
//    	Log.e("Pankaj Rathi", "Fail");
    	database.insert(TABLE_2, null, values);   	
    	
    }
    
    public Cursor getAllOrderDtl () {
    	SQLiteDatabase database = this.getReadableDatabase();
    	return database.rawQuery("Select * from " + TABLE_2, null) ;
    }    
}
