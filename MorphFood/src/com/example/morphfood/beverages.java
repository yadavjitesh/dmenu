package com.example.morphfood;

//import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class beverages extends MainActivity implements  // Extending MainActivity instead of ACtivity to display 
OnItemClickListener {                                   // icon in action bar without coding extra code.

//	private Cursor CusrBvrgs;
	private MyDatabase db;	
	
	ListView listView;
//	List<menuItem> MenuItems;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.morph_listview);
		
		listView = (ListView) findViewById(R.id.list);
		
		db = new MyDatabase(this);
		Cursor CusrBvrgs = db.getAllTable_1();
		MorphDataAdapter adapter = new MorphDataAdapter(this, CusrBvrgs);
		listView.setAdapter(adapter);	
//		@SuppressWarnings("unused")
//		OnItemClickListener itemClickListener = new OnItemClickListener() {
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Cursor cursor_click = (Cursor) arg0.getItemAtPosition(arg2);
				String dish_name = cursor_click.getString(cursor_click.getColumnIndex("DishName"));
				String dish_short_desc = cursor_click.getString(cursor_click.getColumnIndex("ShortDesc"));
				String dish_price = cursor_click.getString(cursor_click.getColumnIndex("DishPrice"));
				String dish_long_desc = cursor_click.getString(cursor_click.getColumnIndex("LongDesc"));
				String dish_image = cursor_click.getString(cursor_click.getColumnIndexOrThrow("DishPic"));
				Intent intent = new Intent(getBaseContext(), dish_details_intent.class);  // this will not work as it will pass context onActionItemClickListener()
				intent.putExtra("IDishName", dish_name);
				intent.putExtra("IDishSDesc", dish_short_desc);
				intent.putExtra("IDishPrice", dish_price);
				intent.putExtra("IDishLDesc", dish_long_desc);
				intent.putExtra("IDishImage", dish_image);
				startActivity(intent);
								
			}
			
		});

	}
		
		
//	@Override
//	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
//		// TODO Auto-generated method stub
//		Log.e("Pankaj Rathi", "Entered");
//		Cursor cursor_click = (Cursor) arg0.getItemAtPosition(arg2);
//		String dish_name = cursor_click.getString(cursor_click.getColumnIndex("DishName"));
//		String dish_short_desc = cursor_click.getString(cursor_click.getColumnIndex("ShortDesc"));
//		String dish_price = cursor_click.getString(cursor_click.getColumnIndex("DishPrice"));
//		String dish_long_desc = cursor_click.getString(cursor_click.getColumnIndex("LongDesc"));
//		String dish_image = cursor_click.getString(cursor_click.getColumnIndexOrThrow("DishPic"));
//		Intent intent = new Intent(this, dish_details_intent.class);
//		intent.putExtra("IDishName", dish_name);
//		intent.putExtra("IDishSDesc", dish_short_desc);
//		intent.putExtra("IDishPrice", dish_price);
//		intent.putExtra("IDishLDesc", dish_long_desc);
//		intent.putExtra("IDishImage", dish_image);
//		startActivity(intent);
//		
//		
//		
//		
//		
//	}	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


@Override
public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
	// TODO Auto-generated method stub
	
}	
}
