-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 18, 2015 at 02:25 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `yo_1`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `dish_category` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`dish_category`) VALUES
('Appetizers'),
('Breads'),
('Curries'),
('Desserts'),
('Sides'),
('Others');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `dish_id` int(6) NOT NULL,
  `dish_name` varchar(100) CHARACTER SET ascii NOT NULL,
  `description` varchar(500) CHARACTER SET armscii8 NOT NULL,
  `ingredients_flag` bit(1) NOT NULL,
  `ingredients` varchar(500) CHARACTER SET armscii8 NOT NULL,
  `dish_nutritional_value_flag` bit(1) NOT NULL,
  `dish_nutritional_value` varchar(500) CHARACTER SET armscii8 NOT NULL,
  `dish_price` int(5) NOT NULL,
  `dish_type_flag` bit(1) NOT NULL,
  `dish_image` varchar(500) CHARACTER SET armscii8 NOT NULL,
  `dish_taste_flag` bit(1) NOT NULL,
  `dish_availability_flag` bit(1) NOT NULL,
  `dish_subcategory` varchar(100) CHARACTER SET armscii8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
